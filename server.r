function(input, output,session) {


     color_func <- colorRampPalette(c('#00ba38','#ff1100'))
     COLOR_1  <- '#00ba38'
     COLOR_2 <- '#ff1100'


     updateLayout <- function(fig, y.axis.title, tickprefix, showlegend = TRUE, rangemode = "tozero", barmode = 'stack', tickformat = NULL, hoverformat = ",.0f") {
		fig <- fig %>% layout(
			showlegend = showlegend
			, barmode = barmode
			, margin = list(
				b = 0,
				l = 80,
				pad = 30
			)
			, xaxis = list(
				title = ""
				, showgrid = FALSE
				, font = list(family = "Inter")
				, color = "#919EAB"
				, gridcolor = "#637381"
				, rangeslider = list(visible = F)
				, tickformat="%b %d"
			)
			, yaxis= list(
				title = list(
					text = y.axis.title,
					standoff = 30
				)
				, color = "#919EAB"
				, gridcolor = "#637381"
				, rangemode = rangemode
				, tickprefix = tickprefix
				, tickformat = tickformat
				, hoverformat = hoverformat
			)
			, plot_bgcolor = plotly.style$plot_bgcolor
			, paper_bgcolor = plotly.style$paper_bgcolor
			, legend = list(font = list(color = 'white'))
		) %>%
		plotly::config(displayModeBar = FALSE) %>%
		plotly::config(modeBarButtonsToRemove = c("zoomIn2d", "zoomOut2d"))
		return(fig)
	}

	renderFig <- function(dt, x.val, y.val, color, x.axis.title, y.axis.title, type, mode, tickprefix="", fill = NULL, stackgroup = NULL, tickformat=NULL, hoverformat = ",.0f") {
		mode <- ifelse( type == 'box', NULL, mode )
		barmode <- ifelse( type == 'box', NULL, 'stack' )
		colors <- color_func(length(unique(dt[, get(color)])))

		if( type %in% c('box','bar') ) {
			fig <- plot_ly(
				data = dt,
				x = ~get(x.val),
				y = ~get(y.val),
				type = type,
				stackgroup = stackgroup,
				color = ~get(color),
				alpha = 1,
				opacity = 1,
				fill = fill,
				colors = colors
			)
		} else {
			fig <- plot_ly(
				data = dt,
				x = ~get(x.val),
				y = ~get(y.val),
				type = type,
				mode = mode,
				stackgroup = stackgroup,
				color = ~get(color),
				alpha = 1,
				opacity = 1,
				fill = fill,
				colors = colors
			)
		}
		fig <- updateLayout(fig, y.axis.title, tickprefix, tickformat=tickformat, barmode = barmode, hoverformat = hoverformat)
		return(fig)
	}



    formatNum <- function(num) {
        if (num < 1000000) {
            return(format(num, big.mark=',', nsmall=0, digits=3))
        } else if (num < 1000000000) {
            return(paste0(format(num / 1000000, big.mark=',', nsmall=0, digits=3),'M'))
        } else {
            return(paste0(format(num / 1000000000, big.mark=',', nsmall=0, digits=3),'B'))
        }
    }





  output$profitPlot <- renderPlotly({
    rishi <- rishi[purchaser %in% eval(input$addy)]
     rishi$type <- ifelse(rishi$reselling_money_made < 0, "below", "above")
     fig <- plot_ly(
          data = rishi,
          x = rishi$reselling_money_made,
          y = rishi$label,
          type = "bar",
		  color = ~type == 'below', colors = c('#00ba38', '#ff1100')
  )

      fig <- updateLayout(fig,'Collection Profit','', showlegend = FALSE, rangemode = "tozero", barmode ='stack',tickformat = '', hoverformat = '' )
    
})

output$inputaddy <- renderUI({
    url.addy <- parseQueryString(session$clientData$url_search)
    
    if(length(url.addy) > 0) {
      default.addy <- names(url.addy)[1]
    } else {
      default.addy <- 'DeoGugXHAjiwTDmM5ab3b1RMZqZPZEzgGjb9aQG15Cwc'
    }
    
    if(is.na(default.addy)) {
      textInput(inputId = "addy", 
                label = NULL,
                width = "100%",
                placeholder = "a SOL wallet address")
    } else {
      textInput(inputId = "addy", 
                label = NULL,
                width = "100%",
                value = '5JiRY4c8YgH1aJCBrWj5gvxv2GF55FKZ3WedJ9rjxGGB')  
    }
  })






   output$total_holds <- renderText({
     profit_selected <- profit[people %in% eval(input$addy)]
     if (nrow(profit_selected) > 0) {
            t <- formatNum(profit_selected$holds)
        }
     paste0(t)
    })

       output$total_resold <- renderText({
           profit_selected2 <- profit[people %in% eval(input$addy)]
     if (nrow(profit_selected2) > 0) {
            t <- formatNum(profit_selected2$resold)
        }
     paste0(t)
    })



   output$total_profit <- renderText({
     profit_selected3 <- profit[people %in% eval(input$addy)]
     if (nrow(profit_selected3) > 0) {
            t <- formatNum(profit_selected3$total)
        }
     paste0(t)
    })



	

	


output$waterfallPlot <-renderPlotly({
    table <- table[purchaser %in% eval(input$addy)]
     fig <- plot_ly(
  table, name = "20", type = "waterfall",
  x = table$date,textposition = "outside", y= table$money,
  connector = list(line = list(color= "rgb(63, 63, 63)"))) 
fig <- fig %>%
  layout(title = " ",
         xaxis = list(title = ""),
         yaxis = list(title = ""),
         autosize = TRUE,
         showlegend = TRUE)
 fig <- updateLayout(fig,'Title of Waterfall Profit','', showlegend = FALSE, rangemode = "tozero", barmode ='stack',tickformat = '', hoverformat = '' )
     
})



output$new_areaPlot <- renderPlotly({
     resold <- resold[seller %in% eval(input$addy)]
     held_mints <- held_mints[purchaser %in% eval(input$addy)]
     held_nfts <- held_nfts[purchaser %in% eval(input$addy)]
     fig <- plot_ly(x = ~resold$date, y = ~resold$resold_p, type = 'scatter', mode = 'lines', name = 'Profit From Reselling', fill = 'tozeroy',
               fillcolor = '#8aff7a',
               line = list(width = 0.5))
fig <- fig %>% add_trace(x = ~held_nfts$date, y = ~held_nfts$held_nfts_p, name = "NFT's value", fill = 'tozeroy',
                         fillcolor = '#7accff')
fig <- fig %>% add_trace(x = ~held_mints$date, y = ~held_mints$held_mints_p, name = "Minted NFT's value", fill = 'tozeroy',
                         fillcolor = '#ff88fb')
fig <- updateLayout(fig,'Money in SOL','', showlegend = TRUE, rangemode = "tozero", barmode ='stack',tickformat = '', hoverformat = '' )

})



output$first_tx <- renderText({
     transactions <- transactions[person %in% eval(input$addy)]
     if (nrow(profit) > 0) {
            t <- formatNum(transactions$first_day)
        }
     paste0(t)
    })
output$last_tx <- renderText({
     transactions <- transactions[person %in% eval(input$addy)]
     if (nrow(profit) > 0) {
            t <- formatNum(transactions$most_recent)
        }
     paste0(t)
    })
output$highest_tx <- renderText({
     transactions <- transactions[person %in% eval(input$addy)]
     if (nrow(profit) > 0) {
            t <- formatNum(transactions$highest_txs_in_a_day)
        }
     paste0(t)
    })

output$txPlot <- renderPlotly({
     calendar <- calendar[person %in% eval(input$addy)]
     fig <- plot_ly(
          data = calendar,
          x = calendar$days,
          y = calendar$txs,
          type = "bar",
          color =I('#00ba38')
  )

      fig <- updateLayout(fig,'Transactions','', showlegend = FALSE, rangemode = "tozero", barmode ='',tickformat = '', hoverformat = '' )
    
})


output$marketPlot <- renderPlotly({
     filtered_market <- market[purchaser %in% eval(input$addy)]
     colors <- c('#B7FFBF','#95F985','#4DED30','#26D701','#00C301','#00AB08')
fig <- plot_ly(data = filtered_market, labels = ~marketplace, values = ~number, type = 'pie',textposition = 'inside',
        textinfo = 'label+percent',
marker = list(colors = colors,
                      line = list(color = '#FFFFFF', width = 1)),
                      #The 'pull' attribute can also be used to create space between the sectors
        showlegend = FALSE)

 fig <- updateLayout(fig,'Markets','', showlegend = FALSE, rangemode = "tozero", barmode ='',tickformat = '', hoverformat = '' )






})















}