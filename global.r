library(data.table)
library(shiny)
library(ggplot2)
library(tools)
library(plotly)
library(shinyBS)
library(shinyjs)
library(htmlwidgets)
library(reactable)
library(stringr)
library(fst)
library(tidyfst)
rishi <- read_fst("rishi.fst", as.data.table = TRUE)
profit <- read_fst("profit.fst", as.data.table = TRUE)
table <- read_fst("table.fst", as.data.table = TRUE)
resold <- read_fst("resold.fst", as.data.table = TRUE)
held_nfts <- read_fst("held_nfts.fst", as.data.table = TRUE)
held_mints <- read_fst("held_mints.fst", as.data.table = TRUE)
transactions <- read_fst("transactions.fst", as.data.table = TRUE)
calendar <- read_fst("calendar.fst", as.data.table = TRUE)
market <- read_fst("market.fst", as.data.table = TRUE)



plotly.style <- list(
  plot_bgcolor = "rgba(0, 0, 0, 0)", 
  paper_bgcolor = "rgba(0, 0, 0, 0)"
)



